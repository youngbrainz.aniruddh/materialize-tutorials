# [Materialize](https://materialize.com) Tutorials

This is a collection of my personal Materialize demos and tutorials.

Materialize is a streaming database for real-time analytics. It was launched in 2019 to address the growing need for the ability to build real-time applications easily and efficiently on streaming data so that businesses can obtain actionable intelligence from streaming data.

Materialize is built upon [Timely Dataflow](https://github.com/TimelyDataflow/timely-dataflow) and [Differential Dataflow](https://github.com/TimelyDataflow/differential-dataflow).

![Materialize Tutorials](https://user-images.githubusercontent.com/21223421/144993631-8234a2e5-d7ef-47e8-af86-30b0fb1205d2.png)

---

## Tutorials list

* [Learn Materialize by running streaming SQL on your nginx logs](./mz-nginx-logs)
* [How to join MySQL and Postgres in a live materialized view](./mz-join-mysql-and-postgresql)
* [Using Materialize and Airbyte with MySQL and Redpanda/Kafka](./mz-airbyte-demo)
* [Using Materialize and LogicLoop](./mz-logicloop-demo)

### dbt

* [Materialize + dbt: User Reviews Demo](./mz-user-reviews-dbt-demo)
* How to create abandoned cart alerts with Materialize

### Node.js / Adonis.js

* [Building a real-time web application with Materialize and AdonisJS](./mz-adonis-demo)
* [Materialize - Raspberry Pi Temperature Sensors Demo](./mz-raspberry-pi-temperature)
* [Materialize - App Shortener App with Upstash Serverless Redis and Kafka + Cloudflare Worker](https://github.com/bobbyiliev/cf-url-shortener)
* [Materialize - Order Tracking Demo App with a live dashboard](./mz-order-tracking-dashboard/)
* Coming soon: [Materialize and Postgres.js - PostgreSQL client for Node.js and Deno](./mz-deno-postgres-js)

### Python / FastAPI

* [Materialize + FastAPI Demo](./mz-fastapi-demo)

### PHP / Laravel

* [Materialize + Laravel EventStream Demo](./laravel-eventstreams)
* [Materialize Migrations + Laravel Zero](./mz-laravel-zero-migrations)

## Useful links

* [Materialize Cloud](https://materialize.com/cloud)
* [Official Materialize documentation](https://materialize.com/docs)
* [Materialize GitHub repository](https://github.com/MaterializeInc/Materialize)
* [Materialize website](https://materialize.com)
* [Official Materialize demos](https://materialize.com/docs/demos)
* [Materialize white paper](https://materialize.com/resources/materialize-an-overview/)

## Contributions:

Any contributions are welcome! If you notice a bug or have a feature request, please open an issue or pull request.

